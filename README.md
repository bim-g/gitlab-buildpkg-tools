Gitlab-buildpkg-tools
======

__Gitlab-buildpkg-tools__ is a set of tools to build a ["Gitlab-PPA"][ppa] using [GitLab CI][ci],
with automatic package rebuild triggered by a push/merge on a branch of your own repository.

After a simple configuration step on your own Gitlab-hosted project, whenever you push on master, 
a pipeline is triggered which distributes the build of packages on docker images that use the 
present tools.

Then, the artefacts produced are collected, signed, and hosted on the Gitlab-Pages of your project 
under a structure usable remotely by APT or YUM.

Currently, the tools produce .deb and .rpm packages for the following systems:

- Debian: jessie, stretch, buster, bullseye, bookworm
- Ubuntu: trusty, xenial, bionic, focal, jammy, kinetic, lunar
- Centos: 6, 7, 8
- Fedora: 24, 25, 26, 27, 28, 29, 30
- Rockylinux: 8, 9


## Quickstart

- Create a project on Gitlab and import your sources.

- Prepare *native* Debian packaging using debmake or similar utility.

- Create a new, dedicated GPG key pair *without passphrase*, and export the 
  private key as ASCII armored output. This key will be used to sign packages.

- On your project page, under the "Settings > CI/CD Pipelines", create a 
  *secret variable* called `GPG_PRIVATE_KEY` and copy/paste the private key
  in the value field.

- Create a second secret variable called `SIGN_USER`, whose value will be
  the `user_ID` of your private key.

- Choose a configuration in the samples directory and copy it at the root
  of your project, named `.gitlab-ci.yml`.

- commit and push to master.

The packages are automatically built by Gitlab-CI, then hosted on Gitlab-Pages.
The access link for the packages is available under the "Settings > Pages" section
of your project.

## Hints

- Do not commit the private key into the source tree.
- If you already have static pages hosted by Gitlab-Pages, they will be erased.
- The .gitlab-ci.yml in this current directory is a special one used to build the
tools recursively from official Docker images. Use samples/ files instead. 
- DO NOT commit the private key into the source tree.


## License

Copyright (c) 2017 Orange

This code is released under the GPLv2 license. See the `LICENSE` file for more information.

## Contact
* Homepage: [opensource.orange.com](http://opensource.orange.com/)

[ci]: https://about.gitlab.com/gitlab-ci/
[ppa]: http://Orange-OpenSource.gitlab.io/gitlab-buildpkg-tools/
