%define package yum-add-gitlab
%define debug_package %{nil}

Summary:	Manage Gitlab repositories configuration
Name:		yum-add-gitlab
Version:	0.1
Release:	1%{?dist}
Group:		Development/Tools
License:	GPL
URL:		https://gitlab.com/Orange-OpenSource/gitlab-buildipkg-tools
Vendor:		Orange
Source0:	%{package}-%{version}.tar.gz
Requires:	curl, rpm, yum
Conflicts:	orange-gitlab-build-tools
BuildRoot:	%{_tmppath}/%{package}-%{version}-%{release}-buildroot
BuildArch:	noarch

%description
 A simple script to add/remove Gitlab repositories to YUM configuration.

%prep
%setup -qn %{package}-%{version}

%build
%{__make}

%install
%{__rm} -rf %{buildroot}
%{__make} DESTDIR=%{buildroot} install

%clean
rm -rf %{buildroot}

%post

%preun

%files
%defattr(-,root,root)
%{_bindir}/*

%changelog
* Wed Jun 7 2017 Michel Decima michel.decima@orange.com 0.1
- Initial package.

