%define package gitlab-buildpkg-tools
%define debug_package %{nil}


Summary:	Gitlab tools for package build
Name:		gitlab-buildpkg-tools
Version:	0.2
Release:	1%{?dist}
Group:		Development/Tools
License:	GPL
URL:		https://gitlab.com/Orange-OpenSource/gitlab-buildpkg-tools
Vendor:         Orange
Packager:       michel.decima@orange.com
Source0:        %{package}-%{version}.tar.gz
Conflicts:      orange-gitlab-build-tools

Requires:       rpm-build, rpmlint, rpmdevtools, redhat-rpm-config, createrepo, tree
%if 0%{?fedora}
Requires:       dnf-plugins-core, rpm-sign
%else
Requires:       yum-utils
%endif
%if 0%{?rhel} >= 7 || 0%{?centos} >= 7
Requires:       rpm-sign
%endif

BuildRequires:	make
BuildRoot:	%{_tmppath}/%{package}-%{version}-%{release}-buildroot
BuildArch:      noarch

%description
 Package to build centos/fedora package using Gitlab and Gitlab CI 
 infrastructure.
 This package is able to build package for all current distro in 
 an easy way using Gitlab Runners

%prep
%setup -qn %{package}-%{version}

%build
%{__make}

%install
%{__rm} -rf %{buildroot}
%{__make} DESTDIR=%{buildroot} install

%clean
rm -rf %{buildroot}

%post

%preun

%files
%defattr(-,root,root)
%{_bindir}/*

%changelog
* Tue May 30 2017 Michel Decima michel.decima@orange.com 0.2
- Stable version with .deb and .rpm build

* Thu May 11 2017 Michel Decima michel.decima@orange.com 0.1
- Initial package.

