#!/bin/bash
# 
#  Copyright (c) 2017 Orange
#  
#  Authors:
#    Christian Bayle <christian.bayle@orange.com>
#    Michel Decima <michel.decima@orange.com>
#  
#  This program is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License version 2 as published 
#  by the Free Software Foundation.
# 

die() {
    echo "${0##*/}: $*" >&2 ; exit 1
}

log_section() {
   printf '=%.0s' {1..72} ; printf "\n"
   printf "=\t%s\n" "" "$@" ""
}

log_subsection() {
   printf "=\t%s\n" "" "$@" ""
}


configure_and_check() {
    RESULT_DIR=${RESULT_DIR:-result}
    PAGES_DIR=${PAGES_DIR:-public}
    
    declare -p RESULT_DIR PAGES_DIR 
    
    [ -z "$RESULT_DIR" ] && die "not set: RESULT_DIR"
    [ -z "$PAGES_DIR" ] && die "not set: PAGES_DIR"
    mkdir -vp "$RESULT_DIR" || die "mkdir failed: $RESULT_DIR"
    mkdir -vp "$PAGES_DIR" || die "mkdir failed: $PAGES_DIR"
}

configure_gpg() {
    GPG_KEY_ID=
    if [ -n "$GPG_PRIVATE_KEY" ] ; then
        log_subsection "Import GPG private key"
        mkdir -p "$HOME/.gitlab" || die
        GNUPGHOME=$(mktemp -d -p "$HOME/.gitlab" -t "gnupg-XXXXXX") || die
        chmod 700 "$GNUPGHOME"
        export GNUPGHOME
        gpg -v --import --batch --no-tty --yes - <<< "$GPG_PRIVATE_KEY" \
            || die "gpg import failed"
        GPG_USER_ID=$(gpg --list-keys --with-colons \
            | grep '^pub\|^uid' | sort | tail -1 | cut -d : -f 10)
        GPG_KEY_ID=$(gpg --list-secret-keys --with-colons \
            | grep ^sec | cut -d : -f 5)
    elif [ -n "$GPG_USER_ID" ] ; then
        log_subsection "Find GPG private key"
        for gpg_dir in $HOME/.gitlab/gnupg $GNUPGHOME $HOME/.gnupg ; do
            [ -f "$gpg_dir/trustdb.gpg" ] || continue
            if gpg --homedir="$gpg_dir" --list-keys "$GPG_USER_ID" &>/dev/null ; then
                export GNUPGHOME="$gpg_dir"
                GPG_KEY_ID=$(gpg --list-secret-keys --with-colons "$GPG_USER_ID" \
                    | grep ^sec | cut -d : -f 5)
                GPG_USER_ID=$(gpg --list-keys --with-colons "$GPG_KEY_ID" \
                    | grep '^pub\|^uid' | sort | tail -1 | cut -d : -f 10)
                break
            fi
        done
    else
        die "use GPG_PRIVATE_KEY (runner) or GPG_USER_ID (workstation)"
    fi
    declare -p GNUPGHOME GPG_KEY_ID GPG_USER_ID
    [ -n "$GPG_KEY_ID" ] || die "missing gpg key-id"
    [ -n "$GPG_USER_ID" ] || die "missing gpg user-id"
}

detect_createrepo() {
    local createrepo_binary=/usr/bin/createrepo
    local createrepo_c_binary=/usr/bin/createrepo_c

    if [ -f "$createrepo_binary" ]; then
        CREATEREPO_BINARY=$createrepo_binary
    elif [ -f "$createrepo_c_binary" ]; then
        CREATEREPO_BINARY=$createrepo_c_binary
    else
        die "unable to detect createrepo binary"
    fi
}

#############################################################################

gpg_export_public_key() {
    local public_key_file="$PAGES_DIR/GPG_PUBLIC_KEY"
    log_subsection "Export GPG public key"
    if [  ! -f "$public_key_file" ] ; then
        gpg -v --export --armor --batch --no-tty --yes \
            --output "$public_key_file" "$GPG_KEY_ID" \
            || die "gpg export failed: $public_key_file"
        [ -s "$public_key_file" ] || die "gpg key not found: $GPG_KEY_ID"
    fi
}

find_releases() {
    (cd "${1:-.}" && find . -maxdepth 2 -mindepth 2 -type d | sed 's/^\.\///')
}

#############################################################################

apt_repo_print_conf() {
    local codename=${1:-foobar}
    cat << EOF
Codename: $codename
Architectures: amd64 source
Components: main
Suite: ${codename}-gitlab
SignWith: $GPG_KEY_ID

EOF
}

apt_repo() {
    log_section "Create APT repositories..."
    echo "releases: $*"
    local apt_dir="$RESULT_DIR/.apt"
    mkdir -v -p "$apt_dir"

    for release in "$@" ; do
        local release_id release_name conf_file
        release_id=$(cut -d/ -f1 <<< "$release")
        release_name=$(cut -d/ -f2 <<< "$release")
        conf_file="$apt_dir/$release_id/conf/distributions"

        if ! grep -q "$release_name" "$conf_file" 2>/dev/null ; then
            mkdir -v -p "$(dirname "$conf_file")"
            printf "%s <-- %s\n" "$conf_file" "$release"
            apt_repo_print_conf "$release_name" >> "$conf_file"
        fi
    done

    tree "$apt_dir"

    for release in "$@" ; do
        log_subsection "include from $RESULT_DIR/$release"
        local release_id release_name pub_dir
        release_id=$(cut -d/ -f1 <<< "$release")
        release_name=$(cut -d/ -f2 <<< "$release")
        pub_dir=$(readlink -e "$PAGES_DIR")

        #for change_file in $RESULT_DIR/$release/*.changes ; do
	# Exclude build-deps package introduced in debian/bullseye
	find "$RESULT_DIR/$release/" -name '*.changes' -not -ipath '*build-deps*' \
            | while read -r change_file ; do
                reprepro --basedir "$apt_dir/$release_id" --verbose \
                    --outdir "$pub_dir/$release_id" \
                    include "$release_name" "$change_file" \
                    || die "reprepro failed: $release"
              done
        reprepro --basedir "$apt_dir/$release_id" \
            list "$release_name"
    done
}

#############################################################################
#
# From:
#   RESULT_DIR/centos/7/*.{noarch,x86_64,src}.rpm
#
# To:
#   PAGES_DIR/centos/7/x86_64/*.{noarch,x86_64}.rpm
#   PAGES_DIR/centos/7/Source/*.src.rpm
#
# Simple repo tree: 'repodata' in rpm folder
#
#

yum_repo() {
    log_section "Create YUM repositories..."
    echo "releases: $*"
    for release in "$@" ; do

        log_subsection "Copy rpms to $PAGES_DIR/$release/x86_64"
        mkdir -v -p "$PAGES_DIR/$release/x86_64"
        find "$RESULT_DIR/$release" \( -name "*.noarch.rpm" -o -name "*.x86_64.rpm" \) -print0 \
            | xargs -0 --no-run-if-empty \
                cp -fv --target-directory "$PAGES_DIR/$release/x86_64" \
                || die "cp failed"

        log_subsection "Copy rpms to $PAGES_DIR/$release/Source"
        mkdir -v -p "$PAGES_DIR/$release/Source"
        find "$RESULT_DIR/$release" -name "*.src.rpm" -print0 \
            | xargs -0 --no-run-if-empty \
                cp -fv --target-directory "$PAGES_DIR/$release/Source" \
                || die "cp failed"
    
        for arch in x86_64 Source ; do
            local repo_dir="$PAGES_DIR/$release/$arch"
            log_subsection "Create yum repo in  $repo_dir"
            $CREATEREPO_BINARY --verbose --outputdir "$repo_dir" "$repo_dir" \
                || die "createrepo failed: $repo_dir"
            gpg --detach-sign --armor -u "$GPG_KEY_ID" \
                --batch --no-tty "$repo_dir/repodata/repomd.xml" \
                || die "gpg failed: $repo_dir/repodata/repomd.xml"
            tree "$repo_dir"
        done
    done
}


#############################################################################


log_section "Configure and check"
configure_and_check
configure_gpg
detect_createrepo

log_section "Artifacts..."
gpg_export_public_key
#rm -rf ${PAGES_DIR:?}/*
tree "$RESULT_DIR/"
tree "$PAGES_DIR/"

log_section "Finding releases..."
APT_RELEASES=() YUM_RELEASES=() UKN_RELEASES=()
while read -r release ; do
    case "$release" in
        debian/*|ubuntu/*|mint/*)             APT_RELEASES+=( "$release" ) ;;
        centos/*|fedora/*|rhel/*|rocky/*)     YUM_RELEASES+=( "$release" ) ;;
        .*)                                   true ;; # skip hidden dirs
        *)                                    UKN_RELEASES+=( "$release" ) ;;
    esac
done < <( find_releases "$RESULT_DIR" | sort -u)
printf "APT: %s\n" "${APT_RELEASES[*]}"
printf "YUM: %s\n" "${YUM_RELEASES[*]}"
printf "UKN: %s\n" "${UKN_RELEASES[*]}"

(( ${#UKN_RELEASES[@]} > 0 )) && die "unknown releases"
(( ${#APT_RELEASES[@]} > 0 )) && apt_repo "${APT_RELEASES[@]}"
(( ${#YUM_RELEASES[@]} > 0 )) && yum_repo "${YUM_RELEASES[@]}"

log_section "Artifacts..."
tree "$PAGES_DIR/"

