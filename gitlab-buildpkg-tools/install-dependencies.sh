#!/bin/bash
# 
#  Copyright (c) 2017 Orange
#  
#  Authors:
#    Christian Bayle <christian.bayle@orange.com>
#    Michel Decima <michel.decima@orange.com>
#  
#  This program is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License version 2 as published 
#  by the Free Software Foundation.
# 

set -e

if [ "$CI_BUILDPKG_BOOTSTRAP" = "yes" ] ; then
    CI_PROJECT_NAMESPACE=${CI_PROJECT_NAMESPACE:-Orange-OpenSource}
    CI_PROJECT_NAME=${CI_PROJECT_NAME:-gitlab-buildpkg-tools}
    #RPM_REPO_ID=$CI_PROJECT_NAMESPACE-$CI_PROJECT_NAME
    RPM_REPO_ID=$(head -1 /etc/yum.repos.d/gitlab*buildpkg*repo| sed 's/\[\(.*\)\]/\1/')

    # RPM repo id used below to disable our own current PPA to avoid
    # chicken-and-egg deadlock on already instrumented docker images
    # when the PPA is down or corrupted.
fi

if [ -x /usr/bin/apt-get ] ; then       # debian|ubuntu|mint

    export DEBIAN_FRONTEND=noninteractive
    apt-get -qq update && apt-get upgrade -y -o Dpkg::Options::="--force-overwrite" || true
    apt-get -q -y -o Dpkg::Options::="--force-overwrite" --no-upgrade install \
        util-linux gnupg git tree curl distro-info openssh-client \
        devscripts equivs libparse-debcontrol-perl dput reprepro alien

    # rpm support made optional because createrepo 0.10.3 is removed from 
    # debian/testing since 2019-10-14.
    # see Bug #938874: yum-metadata-parser: Python2 removal in sid/bullseye
    # https://tracker.debian.org/news/1071555/createrepo-removed-from-testing/
    # handle both cases
    apt-get -q -y -o Dpkg::Options::="--force-overwrite" --no-upgrade install \
        rpm createrepo || true
    apt-get -q -y -o Dpkg::Options::="--force-overwrite" --no-upgrade install \
        rpm createrepo-c || true

    # replace dput with dput-ng if available (>= buster, >= focal)
    apt-get -q -y -o Dpkg::Options::="--force-overwrite" --no-upgrade install \
        dput-ng || true

    # libparse-debcontrol-perl only needed on trusty/jessie, if
    # not present the command 'dch --controlmaint' fails. On recent
    # Debian or Ubuntu version, this dependency is unnecessary.
    # (in debian/control, how to define conditional depends ?)

elif [ -x /usr/bin/dnf ] ; then         # fedora

    DNF_OPTS=()
    if [ "$RPM_REPO_ID" != "" ] ; then
        DNF_OPTS+=( --disablerepo="$RPM_REPO_ID" )
    fi
    # package curl cannot be installed if curl-minimal is already present
    # see https://github.com/rocky-linux/sig-cloud-instance-images/issues/41
    if type curl ; then
        CURL=""
    else
        CURL="curl"
    fi
    dnf -v -y "${DNF_OPTS[@]}" install \
        util-linux gnupg git tree $CURL make \
        rpm rpm-build rpm-sign rpmlint rpmdevtools createrepo \
        redhat-rpm-config dnf-plugins-core

elif [ -x /usr/bin/yum ] ; then         # rhel|centos

    YUM_OPTS=()
    if [ "$RPM_REPO_ID" != "" ] ; then
        YUM_OPTS+=( --disablerepo="$RPM_REPO_ID" )
    fi
    if type curl ; then
        CURL=""
    else
        CURL="curl"
    fi
    yum -v -y "${YUM_OPTS[@]}" install \
        util-linux gnupg git tree $CURL make \
        rpm rpm-build rpm-sign rpmlint rpmdevtools createrepo \
        redhat-rpm-config yum-utils
    
else

    echo  "unkown distribution" ; 
    exit 1

fi

